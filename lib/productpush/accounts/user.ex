defmodule Productpush.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :description, :string
    field :price, :string
    field :product, :string
    field :quantity, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:product, :quantity, :price, :description])
    |> validate_required([:product, :quantity, :price, :description])
  end
end
