use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :productpush, ProductpushWeb.Endpoint,
  secret_key_base: "dEiQU7Zgh+iSSWtpWh+G1bXMSWk9QteqGNfzV88FNd1MnMzu4aSMJsgzMlqDC0VE"

# Configure your database
config :productpush, Productpush.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "productpush_prod",
  pool_size: 15
