defmodule Productpush.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :product, :string
      add :quantity, :string
      add :price, :string
      add :description, :string

      timestamps()
    end

  end
end
